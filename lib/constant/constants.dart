class Constants {
  // Share preferences static keys
  static final baseURL = "baseURL";
  static final lastRequestTime = "lastRequestTime";
  static final listOfErrorCodes = "listOfErrorCodes";
  static final countOfRequest = "countOfRequest";
}
