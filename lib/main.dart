import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:metroui/screens/device/device_monitoring_dashboard.dart';

import 'package:metroui/screens/elevatorescalator/monitoring_dashboard.dart';

void main() {
  // flutter build web --web-renderer html --release //
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeRight]);
  SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
  // Set default home.
  Widget _defaultHome = MonitoringDashboard();
  // Widget _defaultHome = DeviceMonitoringDashboard();
  // Widget _defaultHome = MetroDashboard();
  // Widget _defaultHome = MyHomePage();

  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: "MetroUI",
    theme: ThemeData(primaryColor: Colors.teal),
    home: _defaultHome,
    routes: <String, WidgetBuilder>{
      // Set routes for using the navigator
      // Metro UI (MetroUI Escalator Elevator) (8 Inputs) or (485 Inputs)
      '/dashboard': (BuildContext context) => MonitoringDashboard()
      // Metro UI (MetroUI Lift) (24 Inputs)
      // '/dashboard': (BuildContext context) => DeviceMonitoringDashboard()
      // Metro UI (MetroUI) (485 Inputs)
      // '/dashboard': (BuildContext context) => MetroDashboard()
      // Test
      // '/dashboard': (BuildContext context) => MyHomePage()
    },
  ));
}
