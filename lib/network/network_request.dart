import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:metroui/constant/constants.dart';
import 'package:metroui/model/device_list_model.dart';
import 'package:metroui/model/monitor_model.dart';
import 'package:metroui/model/monitor_report_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NetworkRequestProvider {
  static Future<int> getMonitoringStatus8(String deviceId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int result = 99;
    // String url = (prefs.getString(Constants.baseURL) ?? "");
    String url = "";
    if (url == "") {
      var host = Uri.base.host.toString();
      var port = "5001";
      url = host + ":" + port;
    }

    if (url != "") {
      try {
        final apiResponse = await http.get(
            "http://$url/devices/network/status?device_code=$deviceId&type_=2");
        // final apiResponse =
        //     await http.get("http://192.168.2.132:5000/demo/responseone");
        //print(apiResponse.body);
        if (apiResponse.statusCode == 200) {
          final int Power =
              json.decode(apiResponse.body)['IN1']; // Power On/Off
          final int Up = json.decode(apiResponse.body)['IN2']; //  UP
          final int Down = json.decode(apiResponse.body)['IN3']; // Down
          final int Speed =
              json.decode(apiResponse.body)['IN4']; // Speed High/Low
          final int Fault = json.decode(apiResponse.body)['IN5']; // Under Fault
          final int Maintenace =
              json.decode(apiResponse.body)['IN6']; // Under Maintenance
          final int Emergancy =
              json.decode(apiResponse.body)['IN7']; // Emergency Stop Pressed
          final int Fire =
              json.decode(apiResponse.body)['IN8']; // Fire Mode Activated
          if (Power == 1) {
            // Power Off
            result = 10;
          } else if (Fault == 1) {
            // Fault Alert
            result = 6;
          } else if (Maintenace == 1) {
            // Maintenance Alert
            result = 5;
          } else if (Emergancy == 1) {
            // Emergency Alert
            result = 7;
          } else if (Fire == 1) {
            // Fire Alert
            result = 9;
          } else if (Up == 1 && Speed == 0) {
            // Working OK - Up - High
            result = 0;
          } else if (Up == 1 && Speed == 1) {
            // Working OK - Up - Low
            result = 1;
          } else if (Down == 1 && Speed == 0) {
            // Working OK - Down - High
            result = 2;
          } else if (Down == 1 && Speed == 1) {
            // Working OK - Down - Low
            result = 3;
          } else if (Up == 0 && Down == 0) {
            // Ready To Go
            result = 4;
          } else {
            // Final / Error
          }
        }
      } catch (e) {
        //print(e);
      }
    }
    prefs.setInt(Constants.countOfRequest,
        (prefs.getInt(Constants.countOfRequest) ?? 0) + 1);
    checkCache();
    return result;
  }

  static Future<String> getMonitoringStatus485(String deviceId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String result = "99|NA"; // UI_CODE/ERROR
    // String url = (prefs.getString(Constants.baseURL) ?? "");
    String url = "";
    if (url == "") {
      var host = Uri.base.host.toString();
      var port = "5001";
      url = host + ":" + port;
    }
    if (url != "") {
      try {
        final apiResponse = await http.get(
            "http://$url/devices/network/status?device_code=$deviceId&type_=2");
        // final apiResponse =
        //     await http.get("http://192.168.2.132:5000/demo/response");
        //print(apiResponse.body);
        if (apiResponse.statusCode == 200) {
          final int status_code = json.decode(apiResponse.body)['status_code'];
          final int Up = json.decode(apiResponse.body)['UP'];
          final int Down = json.decode(apiResponse.body)['DOWN'];
          final int Speed = json.decode(apiResponse.body)['SPD'];
          final int Maintenace = json.decode(apiResponse.body)['UM'];
          final int ReadyToStart = json.decode(apiResponse.body)['RTS'];
          String errorCode = "NA";
          Map listOfErrorCodes = json.decode(apiResponse.body);
          listOfErrorCodes.forEach((k, v) {
            // print('{ key: $k, value: $v }');
            if (k != 'status_code' &&
                k != 'UP' &&
                k != 'DOWN' &&
                k != 'SPD' &&
                k != 'UM' &&
                k != 'RTS') {
              if (v == 1) {
                errorCode = k.toString();
                return;
              }
            }
          });
          if (status_code >= 2000) {
            // Communication Error / Power Off
            result = "11|NA";
          } else if (errorCode != "NA") {
            // Any Error From Server
            result = "12|$errorCode";
          } else if (Maintenace == 0) {
            // Maintenance Alert
            result = "5|NA";
          } else if (ReadyToStart == 1) {
            // Ready To Go
            result = "4|NA";
          } else if (Up == 1 && Speed == 0) {
            // Working OK - Up - High
            result = "0|NA";
          } else if (Up == 1 && Speed == 1) {
            // Working OK - Up - Low
            result = "1|NA";
          } else if (Down == 1 && Speed == 0) {
            // Working OK - Down - High
            result = "2|NA";
          } else if (Down == 1 && Speed == 1) {
            // Working OK - Down - Low
            result = "2|NA";
          } else {
            // Final / Error
          }
        }
      } catch (e) {
        //print(e);
      }
    }
    prefs.setInt(Constants.countOfRequest,
        (prefs.getInt(Constants.countOfRequest) ?? 0) + 1);
    checkCache();
    return result;
  }

  static Future<List<MonitoringModel>> getMonitoringList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // String url = (prefs.getString(Constants.baseURL) ?? "");
    String url = ""; //192.168.1.215:5001
    if (url == "") {
      var host = Uri.base.host.toString();
      var port = "5001";
      url = host + ":" + port;
    }
    List<MonitoringModel> list = List();
    if (url != "") {
      try {
        final apiResponse =
            await http.get("http://$url/devices/network/listDevices");
        // print(apiResponse.body);
        if (apiResponse.statusCode == 200) {
          if (json.decode(apiResponse.body) is List) {
            for (var i = 0; i < json.decode(apiResponse.body).length; i++) {
              final String subType =
                  json.decode(apiResponse.body)[i]['sub_type'];
              if (subType.startsWith("ESCL") || subType.startsWith("escl")) {
                final String deviceCode =
                    json.decode(apiResponse.body)[i]['device_code'];
                final String friendlyName =
                    json.decode(apiResponse.body)[i]['name'];
                list.add(MonitoringModel(deviceCode, subType, friendlyName));
              }
            }
          }
        }
      } catch (e) {
        //print(e);
      }
    }
    prefs.setInt(Constants.countOfRequest,
        (prefs.getInt(Constants.countOfRequest) ?? 0) + 1);
    checkCache();
    return list;
  }

  static Future<int> getListOfErrorCodes(String deviceId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // String url = (prefs.getString(Constants.baseURL) ?? "");
    String url = "";
    if (url == "") {
      var host = Uri.base.host.toString();
      var port = "5001";
      url = host + ":" + port;
    }
    var flag = 0;
    Map<String, String> list = Map();
    if (url != "") {
      try {
        final apiResponse =
            await http.get("http://$url/generic_input/get_list/$deviceId");
        // print(apiResponse.body);
        if (apiResponse.statusCode == 200) {
          if (json.decode(apiResponse.body) is List) {
            for (var i = 0; i < json.decode(apiResponse.body).length; i++) {
              final String zone = json.decode(apiResponse.body)[i]['zone'];
              final String name = json.decode(apiResponse.body)[i]['name'];
              list.addAll({'$zone': '$name'});
            }
          }
          flag = 1;
          if (list.length > 0) {
            prefs.setString(
                Constants.listOfErrorCodes, json.encode(list).toString());
          }
        }
      } catch (e) {
        //print(e);
      }
    }
    prefs.setInt(Constants.countOfRequest,
        (prefs.getInt(Constants.countOfRequest) ?? 0) + 1);
    checkCache();
    return flag;
  }

  static Future<List<MonitoringReportModel>> getMonitoringReport(
    String deviceId,
    String startDateTime,
    String endDateTime,
    String forZone,
    bool flagForReport,
  ) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // String url = (prefs.getString(Constants.baseURL) ?? "");
    String url = "";
    if (url == "") {
      var host = Uri.base.host.toString();
      var port = "5001";
      url = host + ":" + port;
    }
    List<MonitoringReportModel> list = List();
    if (url != "") {
      url = "http://$url/report/statechange?start_time=$startDateTime"
          "&end_time=$endDateTime"
          "&device_code=$deviceId"
          "&zone=$forZone"
          "&allzones=$flagForReport";
      try {
        final apiResponse = await http.get(Uri.encodeFull(url));
        // print(apiResponse.body);
        if (apiResponse.statusCode == 200) {
          if (json.decode(apiResponse.body) is List) {
            if (!flagForReport) {
              // Zone wise report
              for (var i = 0; i < json.decode(apiResponse.body).length; i++) {
                final String receive_time =
                    json.decode(apiResponse.body)[i]['receive_time'];
                final String resolve_time =
                    json.decode(apiResponse.body)[i]['resolve_time'];
                final int alarm_time =
                    json.decode(apiResponse.body)[i]['alarm_time'];
                final String name = json.decode(apiResponse.body)[i]['name'];
                final String time_sting =
                    getTimeString(alarm_time, receive_time, resolve_time);
                list.add(MonitoringReportModel("${i + 1}", receive_time,
                    resolve_time, alarm_time, time_sting, name));
              }
            } else {
              int k = 0;
              for (var i = 0; i < json.decode(apiResponse.body).length; i++) {
                for (var j = 0;
                    j < json.decode(apiResponse.body)[i].length;
                    j++) {
                  final String receive_time =
                      json.decode(apiResponse.body)[i][j]['receive_time'];
                  final String resolve_time =
                      json.decode(apiResponse.body)[i][j]['resolve_time'];
                  final int alarm_time =
                      json.decode(apiResponse.body)[i][j]['alarm_time'];
                  final String name =
                      json.decode(apiResponse.body)[i][j]['name'];
                  final String time_sting =
                      getTimeString(alarm_time, receive_time, resolve_time);
                  list.add(MonitoringReportModel("${k + 1}", receive_time,
                      resolve_time, alarm_time, time_sting, name));
                  k++;
                }
              }
            }
          }
        }
      } catch (e) {
        //print(e);
      }
    }
    prefs.setInt(Constants.countOfRequest,
        (prefs.getInt(Constants.countOfRequest) ?? 0) + 1);
    checkCache();
    return list;
  }

  static String getTimeString(
      int value, String receive_time, String resolve_time) {
    if (resolve_time != "NA" &&
        resolve_time != "N/A" &&
        resolve_time != "n/a" &&
        resolve_time != "na") {
      if (value < 60) {
        return "${Duration(seconds: value).inSeconds} Seconds";
      } else if (value > 60 && value < 3600) {
        return "${Duration(seconds: value).inMinutes} Minutes";
      } else {
        return "${Duration(seconds: value).inHours} Hours";
      }
    } else {
      return "NA";
    }
  }

  static Future<int> setFireOnOff(String deviceCode, String method) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // String url = (prefs.getString(Constants.baseURL) ?? "");
    String url = "";
    if (url == "") {
      var host = Uri.base.host.toString();
      var port = "5001";
      url = host + ":" + port;
    }
    int flag = 0;
    if (url != "") {
      try {
        final apiResponse = await http.post(
          "http://$url/outputs/set",
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{
            "device_code": "$deviceCode",
            "zone": "OVRD",
            "method": "$method",
            "options": ""
          }),
        );
        // print(apiResponse.body);
        if (apiResponse.statusCode == 200) {
          flag = 1;
        }
      } catch (e) {
        //print(e);
      }
    }
    prefs.setInt(Constants.countOfRequest,
        (prefs.getInt(Constants.countOfRequest) ?? 0) + 1);
    checkCache();
    return flag;
  }

  static Future<int> setRemoteOnOff(String deviceCode) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // String url = (prefs.getString(Constants.baseURL) ?? "");
    String url = "";
    if (url == "") {
      var host = Uri.base.host.toString();
      var port = "5001";
      url = host + ":" + port;
    }
    int flag = 0;
    if (url != "") {
      try {
        final apiResponse =
            await http.get("http://$url/net_devices/silift$deviceCode");
        // print(apiResponse.body);
        if (apiResponse.statusCode == 200) {
          if (json.decode(apiResponse.body) is List) {
            int status = json.decode(apiResponse.body)['success_code'];
            flag = status;
          }
        }
      } catch (e) {
        //print(e);
      }
    }
    prefs.setInt(Constants.countOfRequest,
        (prefs.getInt(Constants.countOfRequest) ?? 0) + 1);
    checkCache();
    return flag;
  }

  static Future<List<DeviceListModel>> getDeviceList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // String url = (prefs.getString(Constants.baseURL) ?? "");
    String url = "";
    if (url == "") {
      var host = Uri.base.host.toString();
      var port = "5001";
      url = host + ":" + port;
    }
    List<DeviceListModel> list = List();
    if (url != "") {
      try {
        final apiResponse =
            await http.get("http://$url/devices/network/listDevices");
        // print(apiResponse.body);
        if (apiResponse.statusCode == 200) {
          if (json.decode(apiResponse.body) is List) {
            int count = 0;
            for (var i = 0; i < json.decode(apiResponse.body).length; i++) {
              final String subType =
                  json.decode(apiResponse.body)[i]['sub_type'];
              if (subType.startsWith("LIFT") || subType.startsWith("Lift")) {
                final String deviceCode =
                    json.decode(apiResponse.body)[i]['device_code'];
                list.add(DeviceListModel(
                    deviceCode, (count++).toString(), 0.toString()));
              }
            }
          }
        }
      } catch (e) {
        //print(e);
      }
    }
    prefs.setInt(Constants.countOfRequest,
        (prefs.getInt(Constants.countOfRequest) ?? 0) + 1);
    checkCache();
    return list;
  }

  static Future<String> getSubDeviceState(DeviceListModel deviceList) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // String url = (prefs.getString(Constants.baseURL) ?? "");
    String url = "";
    if (url == "") {
      var host = Uri.base.host.toString();
      var port = "5001";
      url = host + ":" + port;
    }
    var status = "99|0";
    if (url != "") {
      try {
        final apiResponse = await http.get(
            "http://$url/devices/network/status?device_code=${deviceList.deviceCode}");
        // print(apiResponse.body);
        if (apiResponse.statusCode == 200) {
          String up = "0";
          String lastUpdateUp = "DD/MM/YYYY";
          String down = "0";
          String lastUpdateDown = "DD/MM/YYYY";
          String fault = "0";
          String lastUpdateFault = "DD/MM/YYYY";
          String floor = "0";
          String lastUpdateFloor = "DD/MM/YYYY";

          if (json.decode(apiResponse.body)['inputs'] is List) {
            for (int i = 0;
                i < json.decode(apiResponse.body)['inputs'].length;
                i++) {
              if (json.decode(apiResponse.body)['inputs'][i]['measurement'] ==
                      "event" ||
                  json.decode(apiResponse.body)['inputs'][i]['measurement'] ==
                      "floor") {
                if (json.decode(apiResponse.body)['inputs'][i]['tags']
                        ['zone'] ==
                    "UP") {
                  up = json
                      .decode(apiResponse.body)['inputs'][i]['fields']['value']
                      .toString();
                  lastUpdateUp = json.decode(apiResponse.body)['inputs'][i]
                      ['tags']['last_recv_dt'];
                }
                if (json.decode(apiResponse.body)['inputs'][i]['tags']
                        ['zone'] ==
                    "DOWN") {
                  down = json
                      .decode(apiResponse.body)['inputs'][i]['fields']['value']
                      .toString();
                  lastUpdateDown = json.decode(apiResponse.body)['inputs'][i]
                      ['tags']['last_recv_dt'];
                }
                if (json.decode(apiResponse.body)['inputs'][i]['tags']
                        ['zone'] ==
                    "FLT") {
                  fault = json
                      .decode(apiResponse.body)['inputs'][i]['fields']['value']
                      .toString();
                  lastUpdateFault = json.decode(apiResponse.body)['inputs'][i]
                      ['tags']['last_recv_dt'];
                }
                if (json.decode(apiResponse.body)['inputs'][i]['tags']
                        ['zone'] ==
                    "FLR") {
                  floor = json
                      .decode(apiResponse.body)['inputs'][i]['fields']['value']
                      .toString();
                  lastUpdateFloor = json.decode(apiResponse.body)['inputs'][i]
                      ['tags']['last_recv_dt'];
                }
              }
            }
          }
          print(
              "Up : $up Down : $down Fault : $fault floor : $floor Device : ${deviceList.deviceCode}");
          print(
              "Up : $lastUpdateUp Down : $lastUpdateDown Fault : $lastUpdateFault floor : $lastUpdateFloor Device : ${deviceList.deviceCode}");
          if (fault == "1") {
            // Fault - Any
            status = "3|$floor";
          } else if (up == "1" && down == "0" && fault == "0") {
            // Working OK - Up
            status = "0|$floor";
          } else if (up == "0" && down == "1" && fault == "0") {
            // Working OK - Down
            status = "1|$floor";
          } else if (up == "0" && down == "0" && fault == "0") {
            // Ready To Go
            status = "2|$floor";
          }
        }
      } catch (e) {
        //print(e);
      }
    }
    prefs.setInt(Constants.countOfRequest,
        (prefs.getInt(Constants.countOfRequest) ?? 0) + 1);
    checkCache();
    return status;
  }

  static checkCache() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if ((prefs.getInt(Constants.countOfRequest) ?? 0) > 200) {
      // _deleteCacheDir();
      // _deleteAppDir();
      prefs.setInt(Constants.countOfRequest, 0);
    }
  }

  static Future<void> _deleteCacheDir() async {
    final cacheDir = await getTemporaryDirectory();
    if (cacheDir.existsSync()) {
      cacheDir.deleteSync(recursive: true);
    }
  }

  static Future<void> _deleteAppDir() async {
    final appDir = await getApplicationSupportDirectory();
    if (appDir.existsSync()) {
      appDir.deleteSync(recursive: true);
    }
  }
}
