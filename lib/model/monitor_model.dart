class MonitoringModel {
  final String deviceCode;
  final String subType;
  final String friendlyName;

  MonitoringModel(
    this.deviceCode,
    this.subType,
    this.friendlyName,
  );

  // Convert JSON sting to Object
  factory MonitoringModel.fromJson(dynamic json) {
    return MonitoringModel(json['deviceCode'] as String,
        json['subType'] as String, json['friendlyName'] as String);
  }

  // Convert Object to JSON String
  @override
  String toString() {
    return "{${this.deviceCode},"
        "${this.subType},"
        "${this.friendlyName}}";
  }

  // Convert Object to JSON
  Map<String, dynamic> toJson() => {
        'deviceCode': deviceCode,
        'subType': subType,
        'friendlyName': friendlyName,
      };
}
