class DeviceListModel {
  final String deviceCode;
  final String devicePosition;
  final String subDeviceNos;

  DeviceListModel(this.deviceCode, this.devicePosition, this.subDeviceNos);

  // Convert JSON sting to Object
  factory DeviceListModel.fromJson(dynamic json) {
    return DeviceListModel(json['deviceCode'] as String,
        json['devicePosition'] as String, json['deviceNos'] as String);
  }

  // Convert Object to JSON String
  @override
  String toString() {
    return "{${this.deviceCode},"
        "${this.devicePosition}"
        "${this.subDeviceNos}}";
  }

  // Convert Object to JSON
  Map<String, dynamic> toJson() => {
        'deviceCode': deviceCode,
        'devicePosition': devicePosition,
        'subDeviceNos': subDeviceNos,
      };
}
