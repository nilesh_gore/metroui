class MonitoringReportModel {
  final String id;
  final String receive_time;
  final String resolve_time;
  final int alarm_time;
  final String time_sting;
  final String zone_name;

  MonitoringReportModel(
    this.id,
    this.receive_time,
    this.resolve_time,
    this.alarm_time,
    this.time_sting,
    this.zone_name,
  );

  // Convert JSON sting to Object
  factory MonitoringReportModel.fromJson(dynamic json) {
    return MonitoringReportModel(
      json['id'] as String,
      json['receive_time'] as String,
      json['resolve_time'] as String,
      json['alarm_time'] as int,
      json['time_sting'] as String,
      json['zone_name'] as String,
    );
  }

  // Convert Object to JSON String
  @override
  String toString() {
    return "{${this.id},"
        "${this.receive_time},"
        "${this.resolve_time},"
        "${this.alarm_time},"
        "${this.time_sting},"
        "${this.zone_name}";
  }

  // Convert Object to JSON
  Map<String, dynamic> toJson() => {
        'id': id,
        'receive_time': receive_time,
        'resolve_time': resolve_time,
        'alarm_time': alarm_time,
        'time_sting': time_sting,
        'zone_name': zone_name,
      };
}
