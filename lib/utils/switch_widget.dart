import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:metroui/model/monitor_model.dart';
import 'package:metroui/network/network_request.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class SwitchWidget extends StatefulWidget {
  final int position;
  final List<MonitoringModel> dataList;

  SwitchWidget(this.position, this.dataList);

  @override
  State<StatefulWidget> createState() {
    return SwitchWidgetState();
  }
}

class SwitchWidgetState extends State<SwitchWidget> {
  bool switchControl = false;
  var titleText = 'Switch is OFF';

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Transform.scale(
          scale: ResponsiveFlutter.of(context).scale(0.5),
          child: Switch(
            onChanged: toggleSwitch,
            value: switchControl,
            activeColor: Colors.white,
            activeTrackColor: Colors.green,
            inactiveThumbColor: Colors.white,
            inactiveTrackColor: Colors.grey,
          ),
        ),
        AutoSizeText(
          "$titleText",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: ResponsiveFlutter.of(context).fontSize(0),
          ),
        )
      ],
    );
  }

  void toggleSwitch(bool flag) {
    if (switchControl == false) {
      switchControl = true;
      titleText = 'Switch is ON';
      setState(() {});
    } else {
      switchControl = false;
      titleText = 'Switch is OFF';
      setState(() {});
    }
  }

  fireOnOff() async {
    // int status = await NetworkRequestProvider.setFireOnOff(
    //     widget.dataList[widget.position].deviceCode);
    // if (status == 0) {
    // } else {}
    // setState(() {});
  }

  remoteOnOff() async {
    int status = await NetworkRequestProvider.setRemoteOnOff(
        widget.dataList[widget.position].deviceCode);
    if (status == 0) {
    } else {}
    setState(() {});
  }
}
