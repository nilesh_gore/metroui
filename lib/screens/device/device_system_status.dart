import 'dart:async';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:metroui/model/device_list_model.dart';
import 'package:metroui/network/network_request.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class DeviceSystemStatus extends StatefulWidget {
  final int position;
  final List<DeviceListModel> dataList;

  DeviceSystemStatus(this.position, this.dataList);

  @override
  State<StatefulWidget> createState() {
    return DeviceSystemStatusState();
  }
}

class DeviceSystemStatusState extends State<DeviceSystemStatus> {
  Timer timer;
  // UP | DOWN | IDLE | FAULT | ERROR
  int mainStatus = 99;
  String response = "99|0";
  String floor = "0";

  @override
  void initState() {
    super.initState();
    fetchData();
    timer = Timer.periodic(Duration(seconds: 1), (Timer t) => fetchData());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: Card(
          elevation: 4,
          child: Padding(
            padding: EdgeInsets.all(2.5),
            child: Column(
              children: [
                Flexible(
                  child: setTitleTextUI(),
                  flex: 1,
                ),
                Flexible(
                  child: Row(
                    children: [
                      Flexible(
                        child: Row(
                          children: [
                            Flexible(
                              child: setLeftSideUI(),
                              flex: 3,
                            ),
                            Flexible(child: setUpDownArrowUI()),
                          ],
                        ),
                        flex: 1,
                      ),
                      Flexible(
                        child: Column(
                          children: [
                            Flexible(child: setUpName()),
                            Flexible(child: setUpMainImage(), flex: 3),
                            Flexible(child: setDeviceNumberTextUI()),
                          ],
                        ),
                        flex: 1,
                      ),
                    ],
                  ),
                  flex: 5,
                ),
              ],
            ),
          )),
    );
  }

  Widget setTitleTextUI() {
    var flag = mainStatus;
    switch (flag) {
      // Working OK - Up
      case 0:
        return Container(
          color: Colors.lightGreen[300],
          child: Center(
            child: AutoSizeText(
              "Working OK",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: ResponsiveFlutter.of(context).fontSize(2),
              ),
            ),
          ),
        );
        break;
      // Working OK - Down
      case 1:
        return Container(
          color: Colors.lightGreen[300],
          child: Center(
            child: AutoSizeText(
              "Working OK",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: ResponsiveFlutter.of(context).fontSize(2),
              ),
            ),
          ),
        );
        break;
      // Ready To Go
      case 2:
        return Container(
          color: Colors.lightGreen[300],
          child: Center(
            child: AutoSizeText(
              "Ready To Go",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: ResponsiveFlutter.of(context).fontSize(2),
              ),
            ),
          ),
        );
        break;
      // Fault Alert
      case 3:
        return Container(
          color: Colors.red[300],
          child: Center(
            child: AutoSizeText(
              "Fault Alert",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: ResponsiveFlutter.of(context).fontSize(2),
              ),
            ),
          ),
        );
        break;
    }
    // Final / Error
    return Container(
      color: Colors.grey[800],
      child: Center(
        child: AutoSizeText(
          "Error",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.red[500],
            fontSize: ResponsiveFlutter.of(context).fontSize(2),
          ),
        ),
      ),
    );
  }

  Widget setLeftSideUI() {
    var flag = mainStatus;
    switch (flag) {
      // Working OK - Up
      case 0:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Floor",
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                    ),
                  ),
                  AutoSizeText(
                    "$floor",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(4),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
            Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  AutoSizeText(
                    "UP",
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 2,
            )
          ],
        ));
        break;
      // Working OK - Down
      case 1:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Floor",
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                    ),
                  ),
                  AutoSizeText(
                    "$floor",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(4),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
            Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  AutoSizeText(
                    "DOWN",
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 2,
            ),
          ],
        ));
        break;
      // Ready To Go
      case 2:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Floor",
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                    ),
                  ),
                  AutoSizeText(
                    "$floor",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(4),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
            Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  AutoSizeText(
                    "Idle",
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 2,
            ),
          ],
        ));
        break;
      // Fault Alert
      case 3:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Floor",
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                    ),
                  ),
                  AutoSizeText(
                    "$floor",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(4),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
            Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  AutoSizeText(
                    "Under Fault",
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 2,
            ),
          ],
        ));
        break;
        break;
    }
    // Final / Error
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        AutoSizeText(
          "Error",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: ResponsiveFlutter.of(context).fontSize(2),
          ),
        ),
        AutoSizeText(
          "",
          style: TextStyle(
            fontSize: ResponsiveFlutter.of(context).fontSize(2),
          ),
        )
      ],
    ));
  }

  Widget setUpDownArrowUI() {
    var flag = mainStatus;
    switch (flag) {
      // Working OK - Up
      case 0:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/up.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/up.png";
              }
            }(),
            height: 100,
            width: 100,
          ),
        );
        break;
      // Working OK - Down
      case 1:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/down.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/down.png";
              }
            }(),
            height: 100,
            width: 100,
          ),
        );
        break;
      // Ready To Go
      case 2:
        return Center();
        break;
      // Fault Alert
      case 3:
        return Center();
        break;
    }
    // Final / Error
    return Center();
  }

  Widget setUpName() {
    if (widget.dataList.length > 0) {
      if (widget.dataList.length >= (widget.position + 1)) {
        return Center(
          child: AutoSizeText(
            "",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
            ),
          ),
        );
      }
    }
    return Center(
      child: AutoSizeText(
        "",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
        ),
      ),
    );
  }

  Widget setUpMainImage() {
    var flag = mainStatus;
    switch (flag) {
      // Working OK - Up
      case 0:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/go_up.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/go_up.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Working OK - Down
      case 1:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/go_down.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/go_down.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Ready To Go
      case 2:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/idle.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/idle.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Fault Alert
      case 3:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/fault.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/fault.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
    }
    // Final / Error
    return Center(
      child: Image.asset(
        () {
          try {
            if (Platform.isIOS || Platform.isAndroid) {
              return "assets/images/not_working.png";
            }
          } catch (e) {
            // print(e);
            // Must be web
            return "images/not_working.png";
          }
        }(),
        height: 300,
        width: 200,
      ),
    );
  }

  Widget setDeviceNumberTextUI() {
    var flag = mainStatus;
    switch (flag) {
      // Working OK - Up
      case 0:
        return Center(
          child: AutoSizeText(
            "Lift ${widget.position + 1}",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: ResponsiveFlutter.of(context).fontSize(2),
            ),
          ),
        );
        break;
      // Working OK - Down
      case 1:
        return Center(
          child: AutoSizeText(
            "Lift ${widget.position + 1}",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: ResponsiveFlutter.of(context).fontSize(2),
            ),
          ),
        );
        break;
      // Ready To Go
      case 2:
        return Center(
          child: AutoSizeText(
            "Lift ${widget.position + 1}",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: ResponsiveFlutter.of(context).fontSize(2),
            ),
          ),
        );
        break;
      // Fault Alert
      case 3:
        return Center(
          child: AutoSizeText(
            "Lift ${widget.position + 1}",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: ResponsiveFlutter.of(context).fontSize(2),
            ),
          ),
        );
        break;
    }
    // Final / Error
    return Center();
  }

  fetchData() async {
    if (widget.dataList.length > 0) {
      try {
        response = await NetworkRequestProvider.getSubDeviceState(
            widget.dataList[widget.position]);
      } catch (e) {
        //print(e);
      }
      mainStatus = int.parse(response.split("|")[0]);
      floor = response.split("|")[1];
      setState(() {});
    }
  }
}
