import 'dart:async';
import 'dart:io';
import 'package:universal_html/html.dart' as html;

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:metroui/model/device_list_model.dart';
import 'package:metroui/network/network_request.dart';
import 'package:metroui/screens/device/device_system_status.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class DeviceMonitoringDashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(color: Colors.grey[400], child: Dashboard()),
      floatingActionButton: FloatingActionButton(
        mini: true,
        onPressed: () {
          var url = "";
          if (url == "") {
            var host = Uri.base.host.toString();
            var port = "6001";
            url = host + ":" + port;
          }
          html.window.open('http://$url', '_self');
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: Colors.teal,
      ),
    );
  }
}

class Dashboard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return DashboardState();
  }
}

class DashboardState extends State<Dashboard> {
  Timer timer;
  List<DeviceListModel> list = List();

  @override
  void initState() {
    super.initState();
    fetchData();
    timer = Timer.periodic(Duration(seconds: 1), (Timer t) => fetchData());
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Flexible(
          child: Container(
            width: double.infinity,
            height: double.infinity,
            child: Card(
              elevation: 4,
              child: Padding(
                padding: EdgeInsets.all(2.5),
                child: Row(
                  children: [
                    Flexible(
                      child: Row(
                        children: [
                          Flexible(
                              child: Center(
                            child: AutoSizeText(
                              "Left",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(2.5),
                              ),
                            ),
                          )),
                          Flexible(
                              child: Row(
                            children: [
                              Center(
                                child: Image.asset(
                                  () {
                                    try {
                                      if (Platform.isIOS ||
                                          Platform.isAndroid) {
                                        return "assets/images/square.png";
                                      }
                                    } catch (e) {
                                      // print(e);
                                      // Must be web
                                      return "images/square.png";
                                    }
                                  }(),
                                  height: 75,
                                  width: 75,
                                ),
                              ),
                              Center(
                                child: AutoSizeText(
                                  "1",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: ResponsiveFlutter.of(context)
                                        .fontSize(2.5),
                                  ),
                                ),
                              )
                            ],
                          )),
                          Flexible(
                              child: Row(
                            children: [
                              Center(
                                child: Image.asset(
                                  () {
                                    try {
                                      if (Platform.isIOS ||
                                          Platform.isAndroid) {
                                        return "assets/images/square.png";
                                      }
                                    } catch (e) {
                                      // print(e);
                                      // Must be web
                                      return "images/square.png";
                                    }
                                  }(),
                                  height: 75,
                                  width: 75,
                                ),
                              ),
                              Center(
                                child: AutoSizeText(
                                  "2",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: ResponsiveFlutter.of(context)
                                        .fontSize(2.5),
                                  ),
                                ),
                              )
                            ],
                          )),
                        ],
                      ),
                      flex: 1,
                    ),
                    Flexible(
                      child: Row(
                        children: [
                          Flexible(
                              child: Center(
                            child: Image.asset(
                              () {
                                try {
                                  if (Platform.isIOS || Platform.isAndroid) {
                                    return "assets/images/left.png";
                                  }
                                } catch (e) {
                                  // print(e);
                                  // Must be web
                                  return "images/left.png";
                                }
                              }(),
                              height: 300,
                              width: 200,
                            ),
                          )),
                          Flexible(
                              child: Center(
                            child: Image.asset(
                              () {
                                try {
                                  if (Platform.isIOS || Platform.isAndroid) {
                                    return "assets/images/iam_icon_small.PNG";
                                  }
                                } catch (e) {
                                  // print(e);
                                  // Must be web
                                  return "images/iam_icon_small.PNG";
                                }
                              }(),
                              height: 125,
                              width: 125,
                            ),
                          )),
                          Flexible(
                              child: Center(
                            child: Image.asset(
                              () {
                                try {
                                  if (Platform.isIOS || Platform.isAndroid) {
                                    return "assets/images/right.png";
                                  }
                                } catch (e) {
                                  // print(e);
                                  // Must be web
                                  return "images/right.png";
                                }
                              }(),
                              height: 300,
                              width: 200,
                            ),
                          )),
                        ],
                      ),
                      flex: 1,
                    ),
                    Flexible(
                      child: Row(
                        children: [
                          Flexible(
                              child: Row(
                            children: [
                              Center(
                                child: Image.asset(
                                  () {
                                    try {
                                      if (Platform.isIOS ||
                                          Platform.isAndroid) {
                                        return "assets/images/square.png";
                                      }
                                    } catch (e) {
                                      // print(e);
                                      // Must be web
                                      return "images/square.png";
                                    }
                                  }(),
                                  height: 75,
                                  width: 75,
                                ),
                              ),
                              Center(
                                child: AutoSizeText(
                                  "3",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: ResponsiveFlutter.of(context)
                                        .fontSize(2.5),
                                  ),
                                ),
                              )
                            ],
                          )),
                          Flexible(
                              child: Row(
                            children: [
                              Center(
                                child: Image.asset(
                                  () {
                                    try {
                                      if (Platform.isIOS ||
                                          Platform.isAndroid) {
                                        return "assets/images/square.png";
                                      }
                                    } catch (e) {
                                      // print(e);
                                      // Must be web
                                      return "images/square.png";
                                    }
                                  }(),
                                  height: 75,
                                  width: 75,
                                ),
                              ),
                              Center(
                                child: AutoSizeText(
                                  "4",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: ResponsiveFlutter.of(context)
                                        .fontSize(2.5),
                                  ),
                                ),
                              )
                            ],
                          )),
                          Flexible(
                              child: Center(
                            child: AutoSizeText(
                              "Right",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(2.5),
                              ),
                            ),
                          )),
                        ],
                      ),
                      flex: 1,
                    ),
                  ],
                ),
              ),
            ),
          ),
          flex: 1,
        ),
        Flexible(
          child: Row(
            children: [
              Flexible(
                child: Container(
                  child: DeviceSystemStatus(0, list),
                ),
                flex: 1,
              ),
              Flexible(
                child: Container(
                  child: DeviceSystemStatus(1, list),
                ),
                flex: 1,
              ),
              Flexible(
                child: Container(
                  child: DeviceSystemStatus(2, list),
                ),
                flex: 1,
              )
            ],
          ),
          flex: 3,
        ),
        Flexible(
          child: Row(
            children: [
              Flexible(
                child: Container(
                  child: DeviceSystemStatus(3, list),
                ),
                flex: 1,
              ),
              Flexible(
                child: Container(
                  child: DeviceSystemStatus(4, list),
                ),
                flex: 1,
              ),
              Flexible(
                child: Container(
                  child: DeviceSystemStatus(5, list),
                ),
                flex: 1,
              )
            ],
          ),
          flex: 3,
        ),
      ],
    );
  }

  fetchData() async {
    if (list.length == 0) {
      List<DeviceListModel> result =
          await NetworkRequestProvider.getDeviceList();
      if (result.length != list.length) {
        list = result;
        setState(() {});
      }
    }
  }
}
