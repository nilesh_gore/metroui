import 'dart:async';

import 'package:flutter/material.dart';
import 'package:metroui/model/device_list_model.dart';
import 'package:metroui/model/monitor_model.dart';
import 'package:metroui/network/network_request.dart';
import 'package:metroui/screens/device/device_system_status.dart';
import 'package:metroui/screens/metro/metro_system_status.dart';

class MetroDashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(color: Colors.grey[400], child: Dashboard()),
      floatingActionButton: FloatingActionButton(
        mini: true,
        onPressed: () {
          // Add your onPressed code here!
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: Colors.teal,
      ),
    );
  }
}

class Dashboard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return DashboardState();
  }
}

class DashboardState extends State<Dashboard> {
  Timer timer;
  List<DeviceListModel> list = List();

  @override
  void initState() {
    super.initState();
    fetchData();
    timer = Timer.periodic(Duration(seconds: 1), (Timer t) => fetchData());
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Flexible(
          child: Row(
            children: [
              Flexible(
                child: Container(
                  child: MetroSystemStatus(0, list),
                ),
                flex: 1,
              ),
              Flexible(
                child: Container(
                  child: MetroSystemStatus(1, list),
                ),
                flex: 1,
              ),
              Flexible(
                child: Container(
                  child: MetroSystemStatus(2, list),
                ),
                flex: 1,
              ),
              Flexible(
                child: Container(
                  child: MetroSystemStatus(3, list),
                ),
                flex: 1,
              )
            ],
          ),
          flex: 4,
        ),
        Flexible(
          child: Row(
            children: [
              Flexible(
                child: Container(
                  child: MetroSystemStatus(4, list),
                ),
                flex: 1,
              ),
              Flexible(
                child: Container(
                  child: MetroSystemStatus(5, list),
                ),
                flex: 1,
              ),
              Flexible(
                child: Container(
                  child: MetroSystemStatus(6, list),
                ),
                flex: 1,
              ),
              Flexible(
                child: Container(
                  child: MetroSystemStatus(7, list),
                ),
                flex: 1,
              )
            ],
          ),
          flex: 4,
        ),
        Flexible(
          child: Container(
            color: Colors.amberAccent[700],
          ),
          flex: 1,
        )
      ],
    );
  }

  fetchData() async {
    if (list.length == 0) {
      List<DeviceListModel> result =
          await NetworkRequestProvider.getDeviceList();
      if (result.length != list.length) {
        list = result;
        setState(() {});
      }
    }
  }
}
