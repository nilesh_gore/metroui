import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dropdown_plus/dropdown_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_web_data_table/web_data_table.dart';
import 'package:metroui/model/monitor_report_model.dart';
import 'package:metroui/network/network_request.dart';
import 'package:metroui/utils/dialogs.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:toast/toast.dart';
import 'package:csv/csv.dart';
import 'package:universal_html/html.dart' as html;

class MonitoringReportList8 extends StatefulWidget {
  final clickedDeviceId;
  final String name;

  MonitoringReportList8(this.clickedDeviceId, this.name);

  @override
  State<StatefulWidget> createState() {
    return MonitoringReportList8State();
  }
}

class MonitoringReportList8State extends State<MonitoringReportList8> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  DateTime currentDate = DateTime.now();
  String _sortColumnName;
  bool _sortAscending;
  List<String> _filterTexts;
  bool _willSearch = true;
  Timer _timer;
  int _latestTick;
  int _rowsPerPage = 10;
  List<MonitoringReportModel> monitoringReportRows = List();
  String selectedDate = "YYYY-MM-DD To YYYY-MM-DD";
  String selectedZone = "";

  @override
  void initState() {
    super.initState();
    _sortColumnName = 'id';
    _sortAscending = true;
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (!_willSearch) {
        if (_latestTick != null && timer.tick > _latestTick) {
          _willSearch = true;
        }
      }
      if (_willSearch) {
        _willSearch = false;
        _latestTick = null;
        setState(() {
          if (_filterTexts != null && _filterTexts.isNotEmpty) {
            _filterTexts = _filterTexts;
            // print('filterTexts = $_filterTexts');
          }
        });
      }
    });
    //fetchData();
  }

  @override
  void dispose() {
    super.dispose();
    _timer?.cancel();
    _timer = null;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Flexible(
          child: Container(
            alignment: Alignment.center,
            child: AppBar(
              backgroundColor: Colors.teal,
              leading: Icon(Icons.table_chart),
              title: Text("Report"),
              actions: [
                IconButton(
                    onPressed: () {
                      getCsvPcm();
                    },
                    tooltip: "Download",
                    icon: Icon(
                      Icons.download_rounded,
                      color: Colors.white,
                    )),
                IconButton(
                    onPressed: () => showAboutUsDialog(context),
                    tooltip: "About",
                    icon: Icon(
                      Icons.contact_support,
                      color: Colors.white,
                    ))
              ],
            ),
          ),
          flex: 1,
        ),
        Flexible(
          child: Container(
              height: double.infinity,
              width: double.infinity,
              child: SingleChildScrollView(
                child: Container(
                  padding: const EdgeInsets.all(8.0),
                  child: WebDataTable(
                    header: Text(''),
                    actions: [
                      Container(
                        width: ResponsiveFlutter.of(context).verticalScale(115),
                        child: TextField(
                          decoration: InputDecoration(
                            prefixIcon: Icon(
                              Icons.search,
                              color: Colors.grey,
                            ),
                            hintText: 'Search...',
                            focusedBorder: const OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Color(0xFFCCCCCC),
                              ),
                            ),
                            border: const OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Color(0xFFCCCCCC),
                              ),
                            ),
                          ),
                          onChanged: (text) {
                            _filterTexts = text.trim().split(' ');
                            _willSearch = false;
                            _latestTick = _timer?.tick;
                          },
                        ),
                      ),
                      Container(
                        height: 50,
                        width: ResponsiveFlutter.of(context).verticalScale(75),
                        child: TextDropdownFormField(
                          options: [
                            "Power Off",
                            "Fault Alert",
                            "Maintenance Alert",
                            "Emergency Button Pressed",
                            "Fire Alert"
                          ],
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              suffixIcon: Icon(Icons.arrow_drop_down),
                              labelText: "Zones"),
                          dropdownHeight: 120,
                          onChanged: (dynamic value) {
                            if (value == "Power Off") {
                              selectedZone = "IN1";
                            } else if (value == "Fault Alert") {
                              selectedZone = "IN5";
                            } else if (value == "Maintenance Alert") {
                              selectedZone = "IN6";
                            } else if (value == "Emergency Button Pressed") {
                              selectedZone = "IN7";
                            } else if (value == "Fire Alert") {
                              selectedZone = "IN8";
                            } else {
                              selectedZone = "";
                            }
                          },
                        ),
                      ),
                      Container(
                        width: ResponsiveFlutter.of(context).verticalScale(115),
                        height: 50,
                        decoration: BoxDecoration(
                          border: Border.all(width: 1.0, color: Colors.grey),
                          borderRadius: BorderRadius.all(Radius.circular(
                                  5.0) //                 <--- border radius here
                              ),
                        ),
                        child: Center(
                          child: Text(
                            '$selectedDate',
                            style: TextStyle(
                                color: Colors.grey[500],
                                fontSize: ResponsiveFlutter.of(context)
                                    .fontSize(1.1)),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 50,
                        width: ResponsiveFlutter.of(context).verticalScale(25),
                        child: IconButton(
                          icon: Icon(
                            Icons.date_range,
                            color: Colors.grey[800],
                          ),
                          onPressed: () async {
                            // _selectDate(context);
                            // final List<DateTime> picked =
                            //     await DateRangePicker.showDatePicker(
                            //         context: context,
                            //         initialFirstDate: new DateTime.now(),
                            //         initialLastDate: (new DateTime.now())
                            //             .add(new Duration(days: 1)),
                            //         firstDate: new DateTime(2021),
                            //         initialDatePickerMode:
                            //             DateRangePicker.DatePickerMode.day,
                            //         lastDate:
                            //             new DateTime(DateTime.now().year + 2));
                            // if (picked != null) {
                            //   // print("Date : $picked");
                            //   if (picked.length == 1) {
                            //     selectedDate =
                            //         "${picked[0].toString().split(" ")[0]}" +
                            //             " To " +
                            //             "${picked[0].toString().split(" ")[0]}";
                            //   } else {
                            //     selectedDate =
                            //         "${picked[0].toString().split(" ")[0]}" +
                            //             " To " +
                            //             "${picked[1].toString().split(" ")[0]}";
                            //   }
                            //   setState(() {});
                            // }
                            DateTimeRange picked = await showDateRangePicker(
                              builder: (context, child) {
                                return Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    ConstrainedBox(
                                      constraints: BoxConstraints(
                                          maxWidth: 300.0, maxHeight: 500.0),
                                      child: child,
                                    )
                                  ],
                                );
                              },
                              context: context,
                              firstDate:
                                  DateTime.now().subtract(Duration(days: 180)),
                              lastDate: DateTime.now().add(Duration(days: 0)),
                              initialDateRange: DateTimeRange(
                                start:
                                    DateTime.now().subtract(Duration(days: 2)),
                                end: DateTime.now(),
                              ),
                            );
                            if (picked != null) {
                              // print(picked.start);
                              // print(picked.end);
                              // print("Date : $picked");
                              // 2021-10-28 00:00:00.000
                              selectedDate =
                                  "${picked.start.toString().split(" ")[0]}" +
                                      " To " +
                                      "${picked.end.toString().split(" ")[0]}";
                              setState(() {});
                            }
                          },
                        ),
                      ),
                      SizedBox(
                        height: 50,
                        width: 75,
                        child: ElevatedButton(
                          child: Text(
                            'Submit',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          onPressed: () async {
                            fetchData();
                          },
                        ),
                      ),
                    ],
                    source: WebDataTableSource(
                      sortColumnName: _sortColumnName,
                      sortAscending: _sortAscending,
                      filterTexts: _filterTexts,
                      columns: [
                        WebDataColumn(
                          name: 'id',
                          label: const Text('Id'),
                          dataCell: (value) => DataCell(Text('$value')),
                        ),
                        WebDataColumn(
                          name: '',
                          label: const Text('Name'),
                          dataCell: (value) => DataCell(Text('${widget.name}')),
                        ),
                        WebDataColumn(
                          name: '',
                          label: const Text('Zone'),
                          dataCell: (value) =>
                              DataCell(Text('${getZoneName()}')),
                        ),
                        WebDataColumn(
                            name: 'receive_time',
                            label: const Text('Occurred DateTime'),
                            dataCell: (value) {
                              if (value is DateTime) {
                                final text =
                                    '${value.year}/${value.month}/${value.day} ${value.hour}:${value.minute}:${value.second}';
                                return DataCell(Text(text));
                              }
                              return DataCell(Text(value
                                  .toString()
                                  .replaceAll("T", " ")
                                  .toString()
                                  .replaceAll("+05:30", "")));
                            },
                            filterText: (value) {
                              if (value is DateTime) {
                                return '${value.year}/${value.month}/${value.day} ${value.hour}:${value.minute}:${value.second}';
                              }
                              return value
                                  .toString()
                                  .replaceAll("T", " ")
                                  .toString()
                                  .replaceAll("+05:30", "");
                            }),
                        WebDataColumn(
                            name: 'resolve_time',
                            label: const Text('Resolved DateTime'),
                            dataCell: (value) {
                              if (value is DateTime) {
                                final text =
                                    '${value.year}/${value.month}/${value.day} ${value.hour}:${value.minute}:${value.second}';
                                return DataCell(Text(text));
                              }
                              return DataCell(Text(value
                                  .toString()
                                  .replaceAll("T", " ")
                                  .toString()
                                  .replaceAll("+05:30", "")));
                            },
                            filterText: (value) {
                              if (value is DateTime) {
                                return '${value.year}/${value.month}/${value.day} ${value.hour}:${value.minute}:${value.second}';
                              }
                              return value
                                  .toString()
                                  .replaceAll("T", " ")
                                  .toString()
                                  .replaceAll("+05:30", "");
                            }),
                        WebDataColumn(
                          name: 'time_sting',
                          label: const Text('Time Difference'),
                          dataCell: (value) => DataCell(Text('$value')),
                        ),
                      ],
                      // rows: SampleData().data,
                      rows: getMapDataFromList(),
                      primaryKeyName: 'id',
                    ),
                    horizontalMargin: 100,
                    onSort: (columnName, ascending) {
                      // print(
                      //     'onSort(): columnName = $columnName, ascending = $ascending');
                      setState(() {
                        _sortColumnName = columnName;
                        _sortAscending = ascending;
                      });
                    },
                    onRowsPerPageChanged: (rowsPerPage) {
                      // print(
                      //     'onRowsPerPageChanged(): rowsPerPage = $rowsPerPage');
                      setState(() {
                        if (rowsPerPage != null) {
                          _rowsPerPage = rowsPerPage;
                        }
                      });
                    },
                    rowsPerPage: _rowsPerPage,
                  ),
                ),
              )),
          flex: 13,
        ),
      ],
    );
  }

  void showToastMessage(String msg, BuildContext context) {
    Toast.show(msg, context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
  }

  Future<void> showAboutUsDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Container(
              constraints: BoxConstraints(
                maxWidth: 300,
                maxHeight: 350,
              ),
              child: Column(
                children: [
                  Image.asset(
                    () {
                      try {
                        if (Platform.isIOS || Platform.isAndroid) {
                          return "assets/images/iam_icon_small.PNG";
                        } else {
                          return "assets/images/iam_icon_small.PNG";
                        }
                      } catch (e) {
                        // print(e);
                        // Must be web
                        return "images/iam_icon_small.PNG";
                      }
                    }(),
                    height: 80,
                    width: 80,
                  ),
                  Text(
                    "Integrated Active Monitoring Pvt. Ltd.",
                    style: TextStyle(color: Colors.black),
                    textAlign: TextAlign.center,
                  ),
                  Divider(
                    color: Colors.transparent,
                  ),
                  Text(
                    "Pune Head Office",
                    style: TextStyle(color: Colors.red),
                    textAlign: TextAlign.center,
                  ),
                  Divider(
                    color: Colors.transparent,
                    height: 2,
                  ),
                  Text(
                    "1st Floor, Block No 27, Electronic Co-op Estate, Satara "
                    "Road, Swargate, Pune – 411009",
                    style: TextStyle(color: Colors.grey),
                    textAlign: TextAlign.center,
                  ),
                  Divider(
                    color: Colors.transparent,
                    height: 2,
                  ),
                  TextButton.icon(
                    label: Text(
                      "+91-020-67479668",
                      style: TextStyle(color: Colors.black),
                    ),
                    icon: Icon(
                      Icons.phone,
                    ),
                  ),
                  TextButton.icon(
                    label: Text(
                      "+91-70451 61874",
                      style: TextStyle(color: Colors.black),
                    ),
                    icon: Icon(
                      Icons.phone,
                    ),
                  ),
                  TextButton.icon(
                    label: Text(
                      "enquiry@smartiam.in",
                      style: TextStyle(color: Colors.black),
                    ),
                    icon: Icon(
                      Icons.email,
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Cancel")),
            ],
          );
        });
  }

  List<Map<String, dynamic>> getMapDataFromList() {
    List<Map<String, dynamic>> data =
        monitoringReportRows.map((row) => row.toJson()).toList();
    return data;
  }

  getCsvPcm() async {
    if (monitoringReportRows.isNotEmpty) {
      String csv;
      List<MonitoringReportModel> list = monitoringReportRows;
      List<List<dynamic>> rows = List<List<dynamic>>();
      for (int i = 0; i < list.length + 1; i++) {
        List<dynamic> row = List();
        if (i == 0) {
          row.add("Id");
          row.add("Name");
          row.add("Zone");
          row.add("Occurred time");
          row.add("Resolved time");
          row.add("Time Difference");
        } else {
          row.add("${list[i - 1].id}");
          row.add("${widget.name}");
          row.add("${getZoneName()}");
          row.add("${list[i - 1].receive_time}");
          row.add("${list[i - 1].resolve_time}");
          row.add("${list[i - 1].time_sting}");
        }
        rows.add(row);
      }
      if (rows.isNotEmpty) {
        csv = const ListToCsvConverter().convert(rows);
        final bytes = utf8.encode(csv);
        final blob = html.Blob([bytes]);
        final url = html.Url.createObjectUrlFromBlob(blob);
        final anchor = html.document.createElement('a') as html.AnchorElement
          ..href = url
          ..style.display = 'none'
          ..download = 'report.csv';
        html.document.body.children.add(anchor);
        anchor.click();
        html.document.body.children.remove(anchor);
        html.Url.revokeObjectUrl(url);
      }
    }
  }

  String getZoneName() {
    if (selectedZone == "IN1") {
      return "Power Off";
    } else if (selectedZone == "IN5") {
      return "Fault Alert";
    } else if (selectedZone == "IN6") {
      return "Maintenance Alert";
    } else if (selectedZone == "IN7") {
      return "Emergency Button Pressed";
    } else if (selectedZone == "IN8") {
      return "Fire Alert";
    } else {
      return "N/A";
    }
  }

  fetchData() async {
    if (selectedZone.isNotEmpty &&
        (selectedDate.isNotEmpty &&
            selectedDate != "YYYY-MM-DD To YYYY-MM-DD")) {
      Dialogs.showLoadingDialog(context, _keyLoader);
      List<MonitoringReportModel> result =
          await NetworkRequestProvider.getMonitoringReport(
              widget.clickedDeviceId,
              selectedDate.split("To")[0].trim() + " 00:00:00",
              selectedDate.split("To")[1].trim() + " 23:59:59",
              selectedZone,
              false);
      monitoringReportRows = result;
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      setState(() {});
      showToastMessage("Data Updated..!!", context);
    } else {
      showToastMessage("Please check Date rage & Zone", context);
    }
  }
}
