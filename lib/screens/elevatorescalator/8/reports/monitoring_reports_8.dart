import 'dart:async';
import 'dart:io';
import 'package:metroui/screens/elevatorescalator/8/reports/monitoring_report_list_8.dart';
import 'package:universal_html/html.dart' as html;

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import 'package:responsive_flutter/responsive_flutter.dart';

class MonitoringReports8 extends StatefulWidget {
  final clickedDeviceId;
  final String name;
  MonitoringReports8(this.clickedDeviceId, this.name);

  @override
  State<StatefulWidget> createState() {
    return MonitoringReports8State();
  }

  static MonitoringReports8State of(BuildContext context) =>
      context.findAncestorStateOfType<MonitoringReports8State>();
}

class MonitoringReports8State extends State<MonitoringReports8> {
  Timer timer;
  List<Widget> screens;
  int selectedScreenPosition = 0;
  int tempPrevious = 0;

  @override
  void initState() {
    super.initState();
    screens = [
      MonitoringReportList8(widget.clickedDeviceId, widget.name),
    ];
    checkUpdates();
    timer = Timer.periodic(Duration(seconds: 1), (Timer t) => checkUpdates());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          Flexible(
            child: Drawer(
              child: ListView(
                padding: EdgeInsets.zero,
                children: [
                  DrawerHeader(
                      decoration: BoxDecoration(
                        color: Colors.teal,
                      ),
                      child: Container(
                        child: Column(
                          children: [
                            Flexible(
                              flex: 2,
                              child: Image.asset(
                                () {
                                  try {
                                    if (Platform.isIOS || Platform.isAndroid) {
                                      return "assets/images/iam_icon_small.PNG";
                                    } else {
                                      return "assets/images/iam_icon_small.PNG";
                                    }
                                  } catch (e) {
                                    // print(e);
                                    // Must be web
                                    return "images/iam_icon_small.PNG";
                                  }
                                }(),
                                height: 80,
                                width: 80,
                              ),
                            ),
                            Flexible(
                                flex: 1,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: AutoSizeText(
                                    "Metro",
                                    style: TextStyle(
                                      fontSize: ResponsiveFlutter.of(context)
                                          .fontSize(1.75),
                                      color: Colors.white,
                                    ),
                                  ),
                                )),
                            Flexible(
                                flex: 1,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: AutoSizeText(
                                    "Reports",
                                    style: TextStyle(
                                      fontSize: ResponsiveFlutter.of(context)
                                          .fontSize(1.00),
                                      color: Colors.white,
                                    ),
                                  ),
                                )),
                          ],
                        ),
                      )),
                  ListTile(
                    title: Text("Report"),
                    onTap: () {
                      selectedScreenPosition = 0;
                      // Navigator.pop(context);
                    },
                  ),
                  ListTile(
                    title: Text("Change UI"),
                    onTap: () {
                      var url = "";
                      if (url == "") {
                        var host = Uri.base.host.toString();
                        var port = "6001";
                        url = host + ":" + port;
                      }
                      html.window.open('http://$url', '_self');
                    },
                  ),
                  ListTile(
                    title: Text("Back To Screen"),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
            flex: 1,
          ),
          Flexible(
            child: getSelectedView(),
            flex: 5,
          )
        ],
      ),
    );
  }

  Widget getSelectedView() {
    if (selectedScreenPosition == 0) {
      screens[selectedScreenPosition] =
          MonitoringReportList8(widget.clickedDeviceId, widget.name);
    }
    tempPrevious = selectedScreenPosition;
    return screens[selectedScreenPosition];
  }

  void checkUpdates() {
    if (!mounted) {
      timer?.cancel();
      return;
    }
    if (tempPrevious != selectedScreenPosition) {
      setState(() {});
    }
  }
}
