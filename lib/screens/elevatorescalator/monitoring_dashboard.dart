import 'dart:async';
import 'package:universal_html/html.dart' as html;

import 'package:flutter/material.dart';
import 'package:metroui/model/monitor_model.dart';
import 'package:metroui/network/network_request.dart';

import 'package:metroui/screens/elevatorescalator/monitoring_system_status.dart';

class MonitoringDashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(color: Colors.grey[400], child: Dashboard()),
      floatingActionButton: FloatingActionButton(
        mini: true,
        onPressed: () {
          var url = "";
          if (url == "") {
            var host = Uri.base.host.toString();
            var port = "6002";
            url = host + ":" + port;
          }
          html.window.open('http://$url', '_self');
        },
        child: const Icon(Icons.navigate_next),
        backgroundColor: Colors.teal,
      ),
    );
  }
}

class Dashboard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return DashboardState();
  }
}

class DashboardState extends State<Dashboard> {
  Timer timer;
  List<MonitoringModel> list = List();

  @override
  void initState() {
    super.initState();
    fetchData();
    timer = Timer.periodic(Duration(seconds: 1), (Timer t) => fetchData());
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Flexible(
          child: Row(
            children: [
              Flexible(
                child: Container(
                  child: MonitoringSystemStatus(0, list),
                ),
                flex: 1,
              ),
              Flexible(
                child: Container(
                  child: MonitoringSystemStatus(1, list),
                ),
                flex: 1,
              ),
              Flexible(
                child: Container(
                  child: MonitoringSystemStatus(2, list),
                ),
                flex: 1,
              )
            ],
          ),
          flex: 1,
        ),
        Flexible(
          child: Row(
            children: [
              Flexible(
                child: Container(
                  child: MonitoringSystemStatus(3, list),
                ),
                flex: 1,
              ),
              Flexible(
                child: Container(
                  child: MonitoringSystemStatus(4, list),
                ),
                flex: 1,
              ),
              Flexible(
                child: Container(
                  child: MonitoringSystemStatus(5, list),
                ),
                flex: 1,
              )
            ],
          ),
          flex: 1,
        ),
      ],
    );
  }

  fetchData() async {
    if (list.length == 0) {
      List<MonitoringModel> result =
          await NetworkRequestProvider.getMonitoringList();
      if (result.isNotEmpty) {
        for (int i = 0; i < result.length; i++) {
          if (result[i].subType == 'ESCL485' ||
              result[i].subType == 'escl485') {
            await NetworkRequestProvider.getListOfErrorCodes(
                result[i].deviceCode);
            break;
          }
        }
      }
      if (result.length != list.length) {
        list = result;
        print("setState() getMonitoringList() getListOfErrorCodes()");
        setState(() {});
      }
    }
  }
}
