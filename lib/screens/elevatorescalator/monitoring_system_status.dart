import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:metroui/constant/constants.dart';
import 'package:metroui/model/monitor_model.dart';
import 'package:metroui/network/network_request.dart';
import 'package:metroui/screens/elevatorescalator/485/reports/monitoring_reports_485.dart';
import 'package:metroui/screens/elevatorescalator/8/reports/monitoring_reports_8.dart';

import 'package:metroui/utils/dialogs.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class MonitoringSystemStatus extends StatefulWidget {
  final int position;
  final List<MonitoringModel> dataList;

  MonitoringSystemStatus(this.position, this.dataList);

  @override
  State<StatefulWidget> createState() {
    return MonitoringSystemStatusState();
  }
}

class MonitoringSystemStatusState extends State<MonitoringSystemStatus> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  Timer timer;

  // UP High | UP Low | DOWN High | DOWN Low | IDLE | Maintenance | Fault
  // | Emergency | Not Working | Fire | Power Off | Communication Error
  // | Any Error From Server | Error
  int mainStatus = 99;
  var lastResult = "99|NA";
  Map listOfErrorCodes = Map();
  var errorCode = "NA";

  @override
  void initState() {
    super.initState();
    fetchData();
    timer = Timer.periodic(Duration(seconds: 1), (Timer t) => fetchData());
    //getErrorListNames();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: Card(
          elevation: 4,
          child: Padding(
            padding: EdgeInsets.all(2.5),
            child: Column(
              children: [
                Flexible(
                  child: setTitleTextUI(),
                  flex: 1,
                ),
                Flexible(
                  child: Row(
                    children: [
                      Flexible(
                        child: Row(
                          children: [
                            Flexible(
                              child: setLeftSideUI(),
                              flex: 3,
                            ),
                            Flexible(child: setUpDownArrowUI()),
                          ],
                        ),
                        flex: 1,
                      ),
                      Flexible(
                        child: Column(
                          children: [
                            Flexible(child: setUpName()),
                            Flexible(child: setUpMainImage(), flex: 3),
                            Flexible(child: setSpeedTextUI()),
                          ],
                        ),
                        flex: 1,
                      ),
                    ],
                  ),
                  flex: 5,
                ),
              ],
            ),
          )),
    );
  }

  Widget setTitleTextUI() {
    var flag = mainStatus;
    switch (flag) {
      // Working OK - Up - High
      case 0:
        return Container(
          color: Colors.lightGreen[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Working OK",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Working OK - Up - Low
      case 1:
        return Container(
          color: Colors.lightGreen[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Working OK",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Working OK - Down - High
      case 2:
        return Container(
          color: Colors.lightGreen[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Working OK",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Working OK - Down - Low
      case 3:
        return Container(
          color: Colors.lightGreen[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Working OK",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Ready To Start
      case 4:
        return Container(
          color: Colors.lightGreen[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Ready To Start",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Maintenance Alert
      case 5:
        return Container(
          color: Colors.red[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Maintenance",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Fault Alert
      case 6:
        return Container(
          color: Colors.red[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Fault",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Emergency Alert
      case 7:
        return Container(
          color: Colors.red[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Emergency",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Not Working
      case 8:
        return Container(
          color: Colors.red[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Not Working",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Fire Alert
      case 9:
        return Container(
          color: Colors.red[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Fire",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Power Off
      case 10:
        return Container(
          color: Colors.grey[800],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Power Status",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.red[500],
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        color: Colors.white,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Communication Error
      case 11:
        return Container(
          color: Colors.grey[800],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Error",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.red[500],
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        color: Colors.white,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Any Error From Server
      case 12:
        return Container(
          color: Colors.grey[800],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "${getErrorTitle()}",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.red[500],
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        color: Colors.white,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
    }
    // Final / Error
    return Container(
      color: Colors.grey[800],
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Align(
              alignment: Alignment.center,
              child: Center(
                child: AutoSizeText(
                  "Error",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.red[500],
                    fontSize: ResponsiveFlutter.of(context).fontSize(2),
                  ),
                ),
              ),
            ),
            flex: 5,
          ),
          Expanded(
              child: Align(
            alignment: Alignment.centerRight,
            child: Center(
              child: IconButton(
                  onPressed: () {
                    nextPage();
                  },
                  icon: Icon(
                    Icons.table_chart,
                    color: Colors.white,
                    size: 30,
                  )),
            ),
          ))
        ],
      ),
    );
  }

  Widget setLeftSideUI() {
    var flag = mainStatus;
    switch (flag) {
      // Working OK - Up - High
      case 0:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "UP",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "0.65 m/s",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Divider(),
                  ),
                  setUpToggleButtonTxt(),
                  setUpToggleButton(),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Working OK - Up - Low
      case 1:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "UP",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "0.5 m/s",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Divider(),
                  ),
                  setUpToggleButtonTxt(),
                  setUpToggleButton(),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Working OK - Down - High
      case 2:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Down",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "0.65 m/s",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Divider(),
                  ),
                  setUpToggleButtonTxt(),
                  setUpToggleButton(),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Working OK - Down - Low
      case 3:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Down",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "0.5 m/s",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Divider(),
                  ),
                  setUpToggleButtonTxt(),
                  setUpToggleButton(),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Ready To Go
      case 4:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Idle",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Divider(),
                  ),
                  setUpToggleButtonTxt(),
                  setUpToggleButton(),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Maintenance Alert
      case 5:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Under Maintenance",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Divider(),
                  ),
                  setUpToggleButtonTxt(),
                  setUpToggleButton(),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Fault Alert
      case 6:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Under Fault",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Divider(),
                  ),
                  setUpToggleButtonTxt(),
                  setUpToggleButton(),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Emergency Alert
      case 7:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Emergency Stop",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "Code : E14",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Divider(),
                  ),
                  setUpToggleButtonTxt(),
                  setUpToggleButton(),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Not Working
      case 8:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Not Working",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Divider(),
                  ),
                  setUpToggleButtonTxt(),
                  setUpToggleButton(),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Fire Alert
      case 9:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Fire Mode Activated",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "Code : E1A",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Divider(),
                  ),
                  setUpToggleButtonTxt(),
                  setUpToggleButton(),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Power Off
      case 10:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Power Off",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Divider(),
                  ),
                  setUpToggleButtonTxt(),
                  setUpToggleButton(),
                ],
              ),
              flex: 3,
            ),
            Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [],
              ),
              flex: 2,
            ),
          ],
        ));
        break;
      // Communication Error
      case 11:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Comm Error",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "Power Off",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Divider(),
                  ),
                  setUpToggleButtonTxt(),
                  setUpToggleButton(),
                ],
              ),
              flex: 3,
            ),
            Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [],
              ),
              flex: 2,
            ),
          ],
        ));
        break;
      // Any Error From Server
      case 12:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "Code : $errorCode",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Divider(),
                  ),
                  setUpToggleButtonTxt(),
                  setUpToggleButton(),
                ],
              ),
              flex: 3,
            ),
            Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [],
              ),
              flex: 2,
            ),
          ],
        ));
        break;
    }
    // Final / Error
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        AutoSizeText(
          "Error",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: ResponsiveFlutter.of(context).fontSize(2),
          ),
        ),
        AutoSizeText(
          "",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: ResponsiveFlutter.of(context).fontSize(2),
          ),
        ),
        setUpToggleButtonTxt(),
        setUpToggleButton(),
      ],
    ));
  }

  Widget setUpToggleButtonTxt() {
    return AutoSizeText(
      "Override",
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: ResponsiveFlutter.of(context).fontSize(1.25),
      ),
    );
  }

  Widget setUpToggleButton() {
    return Flexible(
        child: Row(
      children: [
        Expanded(
          child: ElevatedButton(
            onPressed: () async {
              Dialogs.showLoadingDialog(context, _keyLoader);
              var result = await NetworkRequestProvider.setFireOnOff(
                  widget.dataList[widget.position].deviceCode, "on");
              Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                  .pop();
              if (result == 1) {
                showToastMessage("Sent..!!", context);
              } else {
                showToastMessage("Error..!!", context);
              }
            },
            child: Text("Start"),
          ),
        ),
        Expanded(
          child: ElevatedButton(
            onPressed: () async {
              Dialogs.showLoadingDialog(context, _keyLoader);
              var result = await NetworkRequestProvider.setFireOnOff(
                  widget.dataList[widget.position].deviceCode, "off");
              Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                  .pop();
              if (result == 1) {
                showToastMessage("Sent..!!", context);
              } else {
                showToastMessage("Error..!!", context);
              }
            },
            child: Text("Stop"),
          ),
        )
      ],
    ));
  }

  void showToastMessage(String msg, BuildContext context) {
    Toast.show(msg, context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
  }

  Widget setUpDownArrowUI() {
    var flag = mainStatus;
    switch (flag) {
      // Working OK - Up - High
      case 0:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/up.png";
                } else {
                  return "assets/images/up.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/up.png";
              }
            }(),
            height: 100,
            width: 100,
          ),
        );
        break;
      // Working OK - Up - High
      case 1:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/up.png";
                } else {
                  return "assets/images/up.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/up.png";
              }
            }(),
            height: 100,
            width: 100,
          ),
        );
        break;
      // Working OK - Down - High
      case 2:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/down.png";
                } else {
                  return "assets/images/down.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/down.png";
              }
            }(),
            height: 100,
            width: 100,
          ),
        );
        break;
      // Working OK - Down - Low
      case 3:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/down.png";
                } else {
                  return "assets/images/down.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/down.png";
              }
            }(),
            height: 100,
            width: 100,
          ),
        );
        break;
      // Ready To Go
      case 4:
        return Center();
        break;
      // Maintenance Alert
      case 5:
        return Center();
        break;
      // Fault Alert
      case 6:
        return Center();
        break;
      // Emergency Alert
      case 7:
        return Center();
        break;
      // Not Working
      case 8:
        return Center();
        break;
      // Fire Alert
      case 9:
        return Center();
        break;
      // Power Off
      case 10:
        return Center();
        break;
      // Communication Error
      case 11:
        return Center();
        break;
      // Any Error From Server
      case 12:
        return Center();
        break;
    }
    // Final / Error
    return Center();
  }

  Widget setUpName() {
    if (widget.dataList.length > 0) {
      if (widget.dataList.length >= (widget.position + 1)) {
        return Center(
          child: AutoSizeText(
            mainStatus == 11
                ? widget.dataList[widget.position].friendlyName + "(OFF)"
                : widget.dataList[widget.position].friendlyName + "(ON)",
            textAlign: TextAlign.center,
            style: TextStyle(
              // color: Colors.red[600],
              color: mainStatus == 11 ? Colors.red[600] : Colors.green[600],
              fontWeight: FontWeight.bold,
              fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
            ),
          ),
        );
      }
    }
    return Center(
      child: AutoSizeText(
        "",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
        ),
      ),
    );
  }

  Widget setUpMainImage() {
    var flag = mainStatus;
    switch (flag) {
      // Working OK - Up - High
      case 0:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/go_up.png";
                } else {
                  return "assets/images/go_up.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/go_up.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Working OK - Up - Low
      case 1:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/go_up.png";
                } else {
                  return "assets/images/go_up.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/go_up.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Working OK - Down - High
      case 2:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/go_down.png";
                } else {
                  return "assets/images/go_down.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/go_down.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Working OK - Down - Low
      case 3:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/go_down.png";
                } else {
                  return "assets/images/go_down.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/go_down.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Ready To Go
      case 4:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/idle.png";
                } else {
                  return "assets/images/idle.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/idle.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Maintenance Alert
      case 5:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/maintenance.png";
                } else {
                  return "assets/images/maintenance.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/maintenance.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Fault Alert
      case 6:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/fault.png";
                } else {
                  return "assets/images/fault.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/fault.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Emergency Alert
      case 7:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/emergency.png";
                } else {
                  return "assets/images/emergency.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/emergency.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Not Working
      case 8:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/not_working.png";
                } else {
                  return "assets/images/not_working.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/not_working.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Fire Alert
      case 9:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/fire.png";
                } else {
                  return "assets/images/fire.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/fire.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Power Off
      case 10:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/not_working.png";
                } else {
                  return "assets/images/not_working.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/not_working.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Communication Error
      case 11:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/not_working.png";
                } else {
                  return "assets/images/not_working.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/not_working.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Any Error From Server
      case 12:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/not_working.png";
                } else {
                  return "assets/images/not_working.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/not_working.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
    }
    // Final / Error
    return Center(
      child: Image.asset(
        () {
          try {
            if (Platform.isIOS || Platform.isAndroid) {
              return "assets/images/not_working.png";
            } else {
              return "assets/images/not_working.png";
            }
          } catch (e) {
            // print(e);
            // Must be web
            return "images/not_working.png";
          }
        }(),
        height: 300,
        width: 200,
      ),
    );
  }

  Widget setSpeedTextUI() {
    var flag = mainStatus;
    switch (flag) {
      // Working OK - Up - High
      case 0:
        return Center(
          child: AutoSizeText(
            "High Speed",
            style: TextStyle(
              fontSize: ResponsiveFlutter.of(context).fontSize(1.7),
            ),
          ),
        );
        break;
      // Working OK - Up - Low
      case 1:
        return Center(
          child: AutoSizeText(
            "Low Speed",
            style: TextStyle(
              fontSize: ResponsiveFlutter.of(context).fontSize(1.7),
            ),
          ),
        );
        break;
      // Working OK - Down - High
      case 2:
        return Center(
          child: AutoSizeText(
            "High Speed",
            style: TextStyle(
              fontSize: ResponsiveFlutter.of(context).fontSize(1.7),
            ),
          ),
        );
        break;
      // Working OK - Down - Low
      case 3:
        return Center(
          child: AutoSizeText(
            "Low Speed",
            style: TextStyle(
              fontSize: ResponsiveFlutter.of(context).fontSize(1.7),
            ),
          ),
        );
        break;
      // Ready To Go
      case 4:
        return Center();
        break;
      // Maintenance Alert
      case 5:
        return Center();
        break;
      // Fault Alert
      case 6:
        return Center();
        break;
      // Emergency Alert
      case 7:
        return Center();
        break;
      // Not Working
      case 8:
        return Center();
        break;
      // Fire Alert
      case 9:
        return Center();
        break;
      // Power Off
      case 10:
        return Center();
        break;
      // Communication Error
      case 11:
        return Center();
        break;
      // Any Error From Server
      case 12:
        return Center();
        break;
    }
    // Final / Error
    return Center();
  }

  nextPage() {
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //       builder: (context) => MonitoringReports8("AAAAA", "AAAAA")),
    // );
    try {
      if (widget.dataList.length > 0) {
        if (widget.dataList[widget.position].subType == 'ESCL485' ||
            widget.dataList[widget.position].subType == 'escl485') {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => MonitoringReports485(
                    widget.dataList[widget.position].deviceCode,
                    widget.dataList[widget.position].friendlyName)),
          );
        } else if (widget.dataList[widget.position].subType == 'ESCL8' ||
            widget.dataList[widget.position].subType == 'escl8') {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => MonitoringReports8(
                    widget.dataList[widget.position].deviceCode,
                    widget.dataList[widget.position].friendlyName)),
          );
        }
      }
    } catch (e) {
      print(e);
    }
  }

  fetchData() async {
    if (widget.dataList.length > 0) {
      if (widget.dataList.length >= (widget.position + 1)) {
        if (widget.dataList[widget.position].subType == 'ESCL485' ||
            widget.dataList[widget.position].subType == 'escl485') {
          var status = await NetworkRequestProvider.getMonitoringStatus485(
              widget.dataList[widget.position].deviceCode);
          if (status != lastResult) {
            lastResult = status;
            mainStatus = int.parse(lastResult.split("|")[0]);
            errorCode = lastResult.split("|")[1];
            print(
                "setState() getMonitoringStatus485() Result - $lastResult Device - ${widget.dataList[widget.position].deviceCode}");
            print(
                "---------------------------------------------------------------------");
            setState(() {});
          }
          if (listOfErrorCodes.isEmpty) {
            getErrorListNames();
          }
        } else if (widget.dataList[widget.position].subType == 'ESCL8' ||
            widget.dataList[widget.position].subType == 'escl8') {
          var status = await NetworkRequestProvider.getMonitoringStatus8(
              widget.dataList[widget.position].deviceCode);
          if (status.toString() != lastResult) {
            lastResult = status.toString();
            mainStatus = int.parse(lastResult);
            //errorCode = lastResult.split("|")[1];
            print(
                "setState() getMonitoringStatus8() Result - $lastResult Device - ${widget.dataList[widget.position].deviceCode}");
            print(
                "---------------------------------------------------------------------");
            setState(() {});
          }
        }
      }
    }
  }

  void getErrorListNames() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String data = (prefs.getString(Constants.listOfErrorCodes) ?? "");
    if (data.isNotEmpty) {
      listOfErrorCodes = json.decode(data);
    }
  }

  String getErrorTitle() {
    try {
      return listOfErrorCodes[errorCode] != null
          ? listOfErrorCodes[errorCode]
          : "NA";
    } catch (e) {
      print(e);
    }
    return "NA";
  }
}
