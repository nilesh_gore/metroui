import 'dart:async';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:metroui/model/monitor_model.dart';
import 'package:metroui/network/network_request.dart';
import 'package:metroui/screens/elevatorescalator/8/reports/monitoring_reports_8.dart';

import 'package:responsive_flutter/responsive_flutter.dart';

class MonitoringSystemStatus extends StatefulWidget {
  final int position;
  final List<MonitoringModel> dataList;

  MonitoringSystemStatus(this.position, this.dataList);

  @override
  State<StatefulWidget> createState() {
    return MonitoringSystemStatusState();
  }
}

class MonitoringSystemStatusState extends State<MonitoringSystemStatus> {
  Timer timer;

  // UP High | UP Low | DOWN High | DOWN Low | IDLE | Maintenance | Fault
  // | Emergency | Not Working | Fire | Power Off | Communication Error | Error
  int mainStatus = 11;

  @override
  void initState() {
    super.initState();
    fetchData();
    timer = Timer.periodic(Duration(seconds: 1), (Timer t) => fetchData());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: Card(
          elevation: 4,
          child: Padding(
            padding: EdgeInsets.all(2.5),
            child: Column(
              children: [
                Flexible(
                  child: setTitleTextUI(),
                  flex: 1,
                ),
                Flexible(
                  child: Row(
                    children: [
                      Flexible(
                        child: Row(
                          children: [
                            Flexible(
                              child: setLeftSideUI(),
                              flex: 3,
                            ),
                            Flexible(child: setUpDownArrowUI()),
                          ],
                        ),
                        flex: 1,
                      ),
                      Flexible(
                        child: Column(
                          children: [
                            Flexible(child: setUpName()),
                            Flexible(child: setUpMainImage(), flex: 3),
                            Flexible(child: setSpeedTextUI()),
                          ],
                        ),
                        flex: 1,
                      ),
                    ],
                  ),
                  flex: 5,
                ),
              ],
            ),
          )),
    );
  }

  Widget setTitleTextUI() {
    var flag = mainStatus;
    switch (flag) {
      // Working OK - Up - High
      case 0:
        return Container(
          color: Colors.lightGreen[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Working OK",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Working OK - Up - Low
      case 1:
        return Container(
          color: Colors.lightGreen[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Working OK",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Working OK - Down - High
      case 2:
        return Container(
          color: Colors.lightGreen[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Working OK",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Working OK - Down - Low
      case 3:
        return Container(
          color: Colors.lightGreen[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Working OK",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Ready To Go
      case 4:
        return Container(
          color: Colors.lightGreen[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Ready To Start",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Maintenance Alert
      case 5:
        return Container(
          color: Colors.red[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Maintenance",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Fault Alert
      case 6:
        return Container(
          color: Colors.red[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Fault",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Emergency Alert
      case 7:
        return Container(
          color: Colors.red[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Emergency",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Not Working
      case 8:
        return Container(
          color: Colors.red[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Not Working",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Fire Alert
      case 9:
        return Container(
          color: Colors.red[300],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Fire",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Power Off
      case 10:
        return Container(
          color: Colors.grey[800],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Power Status",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.red[500],
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        color: Colors.white,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
      // Communication Error
      case 11:
        return Container(
          color: Colors.grey[800],
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: AutoSizeText(
                      "Communication Error",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.red[500],
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      ),
                    ),
                  ),
                ),
                flex: 5,
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        nextPage();
                      },
                      icon: Icon(
                        Icons.table_chart,
                        color: Colors.white,
                        size: 30,
                      )),
                ),
              ))
            ],
          ),
        );
        break;
    }
    // Final / Error
    return Container(
      color: Colors.grey[800],
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Align(
              alignment: Alignment.center,
              child: Center(
                child: AutoSizeText(
                  "Error",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.red[500],
                    fontSize: ResponsiveFlutter.of(context).fontSize(2),
                  ),
                ),
              ),
            ),
            flex: 5,
          ),
          Expanded(
              child: Align(
            alignment: Alignment.centerRight,
            child: Center(
              child: IconButton(
                  onPressed: () {
                    //nextPage();
                  },
                  icon: Icon(
                    Icons.table_chart,
                    color: Colors.white,
                    size: 30,
                  )),
            ),
          ))
        ],
      ),
    );
  }

  Widget setLeftSideUI() {
    var flag = mainStatus;
    switch (flag) {
      // Working OK - Up - High
      case 0:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "UP",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "0.5 m/s",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Working OK - Up - Low
      case 1:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "UP",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "0.2 m/s",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Working OK - Down - High
      case 2:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Down",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "0.5 m/s",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Working OK - Down - Low
      case 3:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Down",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "0.2 m/s",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Ready To Go
      case 4:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Idle",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Maintenance Alert
      case 5:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Under Maintenance",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Fault Alert
      case 6:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Under Fault",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Emergency Alert
      case 7:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Emergency Stop",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Not Working
      case 8:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Not Working",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Fire Alert
      case 9:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Fire Mode Activated",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
          ],
        ));
        break;
      // Power Off
      case 10:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Power Off",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
            Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [],
              ),
              flex: 2,
            ),
          ],
        ));
        break;
      // Communication Error
      case 11:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "Error",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                  AutoSizeText(
                    "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ],
              ),
              flex: 3,
            ),
            Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [],
              ),
              flex: 2,
            ),
          ],
        ));
        break;
    }
    // Final / Error
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        AutoSizeText(
          "Error",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: ResponsiveFlutter.of(context).fontSize(2),
          ),
        ),
        AutoSizeText(
          "",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: ResponsiveFlutter.of(context).fontSize(2),
          ),
        )
      ],
    ));
  }

  Widget setUpDownArrowUI() {
    var flag = mainStatus;
    switch (flag) {
      // Working OK - Up - High
      case 0:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/up.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/up.png";
              }
            }(),
            height: 100,
            width: 100,
          ),
        );
        break;
      // Working OK - Up - High
      case 1:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/up.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/up.png";
              }
            }(),
            height: 100,
            width: 100,
          ),
        );
        break;
      // Working OK - Down - High
      case 2:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/down.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/down.png";
              }
            }(),
            height: 100,
            width: 100,
          ),
        );
        break;
      // Working OK - Down - Low
      case 3:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/down.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/down.png";
              }
            }(),
            height: 100,
            width: 100,
          ),
        );
        break;
      // Ready To Go
      case 4:
        return Center();
        break;
      // Maintenance Alert
      case 5:
        return Center();
        break;
      // Fault Alert
      case 6:
        return Center();
        break;
      // Emergency Alert
      case 7:
        return Center();
        break;
      // Not Working
      case 8:
        return Center();
        break;
      // Fire Alert
      case 9:
        return Center();
        break;
      // Power Off
      case 10:
        return Center();
        break;
      // Communication Error
      case 11:
        return Center();
        break;
    }
    // Final / Error
    return Center();
  }

  Widget setUpName() {
    if (widget.dataList.length > 0) {
      if (widget.dataList.length >= (widget.position + 1)) {
        return Center(
          child: AutoSizeText(
            widget.dataList[widget.position].friendlyName,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
            ),
          ),
        );
      }
    }
    return Center(
      child: AutoSizeText(
        "",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
        ),
      ),
    );
  }

  Widget setUpMainImage() {
    var flag = mainStatus;
    switch (flag) {
      // Working OK - Up - High
      case 0:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/go_up.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/go_up.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Working OK - Up - Low
      case 1:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/go_up.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/go_up.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Working OK - Down - High
      case 2:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/go_down.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/go_down.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Working OK - Down - Low
      case 3:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/go_down.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/go_down.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Ready To Go
      case 4:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/idle.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/idle.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Maintenance Alert
      case 5:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/maintenance.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/maintenance.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Fault Alert
      case 6:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/fault.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/fault.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Emergency Alert
      case 7:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/emergency.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/emergency.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Not Working
      case 8:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/not_working.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/not_working.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Fire Alert
      case 9:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/fire.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/fire.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Power Off
      case 10:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/not_working.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/not_working.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
      // Communication Error
      case 11:
        return Center(
          child: Image.asset(
            () {
              try {
                if (Platform.isIOS || Platform.isAndroid) {
                  return "assets/images/not_working.png";
                }
              } catch (e) {
                // print(e);
                // Must be web
                return "images/not_working.png";
              }
            }(),
            height: 300,
            width: 200,
          ),
        );
        break;
    }
    // Final / Error
    return Center(
      child: Image.asset(
        () {
          try {
            if (Platform.isIOS || Platform.isAndroid) {
              return "assets/images/not_working.png";
            }
          } catch (e) {
            // print(e);
            // Must be web
            return "images/not_working.png";
          }
        }(),
        height: 300,
        width: 200,
      ),
    );
  }

  Widget setSpeedTextUI() {
    var flag = mainStatus;
    switch (flag) {
      // Working OK - Up - High
      case 0:
        return Center(
          child: AutoSizeText(
            "High Speed",
            style: TextStyle(
              fontSize: ResponsiveFlutter.of(context).fontSize(1.7),
            ),
          ),
        );
        break;
      // Working OK - Up - Low
      case 1:
        return Center(
          child: AutoSizeText(
            "Low Speed",
            style: TextStyle(
              fontSize: ResponsiveFlutter.of(context).fontSize(1.7),
            ),
          ),
        );
        break;
      // Working OK - Down - High
      case 2:
        return Center(
          child: AutoSizeText(
            "High Speed",
            style: TextStyle(
              fontSize: ResponsiveFlutter.of(context).fontSize(1.7),
            ),
          ),
        );
        break;
      // Working OK - Down - Low
      case 3:
        return Center(
          child: AutoSizeText(
            "Low Speed",
            style: TextStyle(
              fontSize: ResponsiveFlutter.of(context).fontSize(1.7),
            ),
          ),
        );
        break;
      // Ready To Go
      case 4:
        return Center();
        break;
      // Maintenance Alert
      case 5:
        return Center();
        break;
      // Fault Alert
      case 6:
        return Center();
        break;
      // Emergency Alert
      case 7:
        return Center();
        break;
      // Not Working
      case 8:
        return Center();
        break;
      // Fire Alert
      case 9:
        return Center();
        break;
      // Power Off
      case 10:
        return Center();
        break;
      // Communication Error
      case 11:
        return Center();
        break;
    }
    // Final / Error
    return Center();
  }

  nextPage() {
    if (widget.dataList.length > 0) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => MonitoringReports8(
                widget.dataList[widget.position].deviceCode,
                widget.dataList[widget.position].friendlyName)),
      );
    }
    /*else {
      var url = "http://google.com/";
      if (url == "") {
        var host = Uri.base.host.toString();
        var port = "5002";
        url = host + ":" + port;
      }
      html.window.open(url, '_self');
    }*/
  }

  fetchData() async {
    if (widget.dataList.length > 0) {
      if (widget.dataList.length >= (widget.position + 1)) {
        var status = await NetworkRequestProvider.getMonitoringStatus8(
            widget.dataList[widget.position].deviceCode);
        if (mainStatus != status) {
          setState(() {});
        }
      }
    }
  }
}
